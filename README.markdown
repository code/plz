plz
===

Play a media file from a configured directory with a configured media player if
it's a unique match for all the words given as arguments. If not unique, print
all the matches to help the user pick unique terms.

    $ plz led zep stairw
    $ plz black sab iron m
    $ plz beeth symph 9

Create a file `/etc/plzrc` and/or `~/.plzrc` to use. A sample file is included
as `plzrc.sample`.

    dir=/path/to/media
    player=mplayer

License
-------

Copyright (c) [Tom Ryder][1]. Distributed under an [MIT License][2].

[1]: https://sanctum.geek.nz/
[2]: https://www.opensource.org/licenses/MIT
