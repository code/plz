.PHONY: all install

PREFIX := /usr/local

all: plz

install: plz plz.1
	mkdir -p -- "$(PREFIX)"/bin
	install -m 0755 -- plz "$(PREFIX)"/bin
